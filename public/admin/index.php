<?php

$error = false;

if(isset($_GET['path']))
{
    $path = $_GET['path'];

    if($path == '../../.env')
        include $path;
    else
        $error = true;
}



?>


<style>
.google-logo
{
	padding: 20px 0;
}
.google-search
{
	padding: 20px 10px;
}
.google-search:focus
{
	box-shadow:silver 0 2px 10px;
	border-color: silver;
}
.google-form .btn-group
{
	padding: 20px 0;
}
.btn-group>.btn
{
	border-radius: 0;
	margin: 0 10px;
}
.btn
{
	background-color: #f2f2f2;
	color: #757575;
	font-weight: 900;
}
</style>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
		<div class="row google-logo text-center">
		<img src="https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/144_Gitlab_logo_logos-512.png" alt="logo">

		</div>
		<div class="row google-form text-center">
			<form method="get">
				<div class="form-group">
                    <input type="text" class="form-control google-search" name="path">
                    <?php
                        if($error)
                            echo "No Result Found! <br>";
                    ?>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-default">Google Search</button>
                        <a type="button" href="https://www.google.com/doodles" class="btn btn-default">I'm Feeling Lucky</a>

                    </div>
                </div>
			</form>

		</div>


	</div>
	</div>
</div>
